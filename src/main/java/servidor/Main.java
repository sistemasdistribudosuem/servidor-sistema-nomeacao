package servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import com.sun.net.httpserver.HttpServer;


public class Main {
	
	private static final String PORTA_DNS = "portaDNS";
	private static final String DNS = "enderecoDNS";
	private static final String PORTA = "porta";
	private static final String NOME_SERIVCO = "nome";

	public static void main(String[] args) throws IOException{
		validaArgumentos();
		
		registraDNS();

		iniciaServico();
	}
	
	private static void validaArgumentos(){
		if(getNomeServico() == null)
			throw new RuntimeException("Informe o nome do serviço com -Dnome.");
		
		if(System.getProperty(PORTA) == null)
			throw new RuntimeException("Informe a porta do serviço com -Dporta.");
		
		if(getEnderecoDNS() == null)
			throw new RuntimeException("Informe o Endereço do DNS -DenderecoDNS.");
		
		if(System.getProperty(PORTA_DNS) == null)
			throw new RuntimeException("Informe a porta do DNS com -DportaDNS.");
	}
	
	private static void registraDNS() throws MalformedURLException, ProtocolException, IOException {
		
		registraDNS(
				getEnderecoDNS(), 
				getPortaDNS(), 
				getNomeServico(), 
				"localhost", 
				getPorta());
		
	}

	private static int getPortaDNS() {
		return Integer.valueOf(System.getProperty(PORTA_DNS));
	}

	private static String getEnderecoDNS() {
		return System.getProperty(DNS);
	}

	private static void iniciaServico() throws IOException {
		String nome = getNomeServico();
		Servico servico = ServicosDisponiveis.getServico(nome);
		
		HttpServer server = HttpServer.create(new InetSocketAddress(getPorta()), 0);
        server.createContext("/", servico);
        server.start();
	}

	private static int getPorta() {
		return Integer.valueOf(System.getProperty(PORTA));
	}

	private static String getNomeServico() {
		return System.getProperty(NOME_SERIVCO);
	}

	private static String registraDNS(
			String enderecoDNS, 
			int portaDNS, 
			String nome, 
			String enderecoServico, 
			int portaServico) {
		
		try{
			StringBuilder result = new StringBuilder();
			
			String urlStr = String.format("http://%s:%d/registra?nome=%s&ip=%s&porta=%d", enderecoDNS, portaDNS, nome, enderecoServico, portaServico);
			
		    URL url = new URL(urlStr);
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("GET");
		    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    String line;
		    while ((line = rd.readLine()) != null) {
		       result.append(line);
		    }
		    rd.close();
		    String response = result.toString();
		    System.out.println(response);
			return response;
		}
	    catch (Exception e) {
	    	throw new RuntimeException(String.format("Erro ao registrar DNS - %s:%d", enderecoDNS, portaDNS));
		}
	}
}
