package servidor;

import java.util.HashMap;
import java.util.Map;

public enum ServicosDisponiveis {
	
	SOMA("soma", new Soma()),
	SUBTRACAO("subtracao", new Subtracao()),
	DIVISAO("divisao", new Divisao()),
	MULTIPLICACAO("multiplicacao", new Multiplicacao());
	
	private Servico servico;
	private String nome;

	private ServicosDisponiveis(String nome, Servico servico) {
		this.nome = nome;
		this.servico = servico;
	}
	
	static Map<String, ServicosDisponiveis> map = new HashMap<>();
	static {
		for(ServicosDisponiveis s : values())
			map.put(s.nome, s);
	}

	public static Servico getServico(String nome) {
		return map.get(nome).servico;
	}

}
