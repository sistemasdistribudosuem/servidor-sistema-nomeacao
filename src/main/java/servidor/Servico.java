package servidor;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class Servico implements HttpHandler {

abstract String getContexto();

	protected double resultado;
	
	abstract void setParametro(String chave, String valor);

	abstract int getQuantidadeParametros();
	
	abstract void executa();

	@Override
	public void handle(HttpExchange args) throws IOException {
		String query = args.getRequestURI().getQuery();
		String[] parametros = query.split("&");
		
		validaQuantidadeParametros(parametros);
		
		for(String param : parametros){
			String[] parametro = param.split("=");
			
			validaChaveValor(parametro);
			
			String chave = parametro[0];
			String valor = parametro[1];
			setParametro(chave, valor);
		}
		
		System.out.println("executando servico");
		executa();
		
		addResponse(args);
	}

	private void validaChaveValor(String[] parametro) {
		if(parametro.length != 2)
			throw new RuntimeException("Informe chave e valor no parâmetro (chave=valor)");
	}

	private void validaQuantidadeParametros(String[] parametros) {
		if(parametros.length < getQuantidadeParametros())
			throw new RuntimeException("Quantidade de parâmetros está incorreta.");
	}
	
	protected void addResponse(HttpExchange args) throws IOException {
		String response = String.valueOf(this.resultado);
		System.out.println("Respondendo resultado: "+response);
        args.sendResponseHeaders(200, response.length());
        OutputStream os = args.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}
}
