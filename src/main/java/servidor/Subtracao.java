package servidor;

import java.util.ArrayList;
import java.util.List;

public class Subtracao extends Servico {
	
	List<Integer> valores = new ArrayList<>();

	@Override
	String getContexto() {
		return "/";
	}

	@Override
	void setParametro(String chave, String valor) {
		this.valores.add(Integer.valueOf(valor));
	}

	@Override
	int getQuantidadeParametros() {
		return 2;
	}

	@Override
	void executa() {
		this.resultado = valores.get(0);
		
		for(int i = 1; i < valores.size() ; i++)
			this.resultado = this.resultado - this.valores.get(i);
		
		this.valores = new ArrayList<>();
		
	}

}
