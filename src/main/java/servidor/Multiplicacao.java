package servidor;

import java.util.ArrayList;
import java.util.List;

public class Multiplicacao extends Servico{

	List<Integer> lista = new ArrayList<>();
	
	@Override
	String getContexto() {
		return "/";
	}

	@Override
	void setParametro(String chave, String valor) {
		lista.add(Integer.valueOf(valor));
	}

	@Override
	int getQuantidadeParametros() {
		return 2;
	}

	@Override
	void executa() {
		this.resultado = lista.get(0);
		lista.remove(0);
		for(Integer i : lista)
			this.resultado = resultado*i;
		
		lista = new ArrayList<>();
		
	}

}
