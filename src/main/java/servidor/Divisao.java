package servidor;

import java.math.BigDecimal;

public class Divisao extends Servico {

	Integer a;
	Integer b;
	
	@Override
	String getContexto() {
		return "/";
	}

	@Override
	void setParametro(String chave, String valor) {
		if(a == null)
			a = Integer.valueOf(valor);
		else
			b = Integer.valueOf(valor);
	}

	@Override
	int getQuantidadeParametros() {
		return 2;
	}

	@Override
	void executa() {
		if(b == 0)
			throw new RuntimeException("Não existe divisão por zero");
		
		this.resultado = new BigDecimal(a).divide(new BigDecimal(b)).doubleValue();
	}

}
