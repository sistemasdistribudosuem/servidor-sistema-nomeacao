package servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
	
	@Test
	public void testSoma() throws IOException{
		String porta = "8001";
		
		System.setProperty("nome", "soma");
		System.setProperty("porta", porta);
		
		Main.main(new String[]{});
		
		String resultado = executa(porta, 4, 5);
	  
		Assert.assertEquals("9.0", resultado);
	}
	
	@Test
	public void testSubtracao() throws IOException{
		String porta = "8002";
		
		System.setProperty("nome", "subtração");
		System.setProperty("porta", porta);
		
		Main.main(new String[]{});
		
		String resultado = executa(porta, 10, 5);
	  
		Assert.assertEquals("5.0", resultado);
	}
	
	@Test
	public void testDivisao() throws IOException{
		String porta = "8003";
		
		System.setProperty("nome", "divisão");
		System.setProperty("porta", porta);
		
		Main.main(new String[]{});
		
		String resultado = executa(porta, 9, 2);
	  
		Assert.assertEquals("4.5", resultado);
	}
	
	@Test
	public void testMultiplicacao() throws IOException{
		String porta = "8004";
		
		System.setProperty("nome", "multiplicação");
		System.setProperty("porta", porta);
		
		Main.main(new String[]{});
		
		String resultado = executa(porta, 9, 2);
	  
		Assert.assertEquals("18.0", resultado);
	}

	private String executa(String porta, int a, int b) throws MalformedURLException, IOException, ProtocolException {
		StringBuilder result = new StringBuilder();
		String urlStr = String.format("http://localhost:%s/?a=%d&b=%d", porta, a, b);
		
	    URL url = new URL(urlStr);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	       result.append(line);
	    }
	    rd.close();
	    return result.toString();
	}

}
